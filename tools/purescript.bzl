def _purescript_bundle(ctx):
    library = ctx.files.library[0]
    purs    = ctx.executable.purs
    js      = ctx.outputs.js
    ctx.actions.run_shell(
        inputs = [library, purs],
        outputs = [js],
        command = """
            set -o errexit
            mkdir output
            tar --extract --file "$1" --directory output
            "$2" bundle output/*/{index,foreign}.js --output "$3"
        """,
        arguments = [library.path, purs.path, js.path],
    )

def _purescript_library(ctx):
    srcs = ctx.files.srcs
    tar  = ctx.outputs.tar
    purs = ctx.executable.purs
    ctx.actions.run_shell(
        inputs = srcs + [purs],
        outputs = [tar],
        command = """
            set -o errexit
            "$1" compile "${@:3}"
            tar --create --file "$2" --directory output .
        """,
        arguments =
            [purs.path, tar.path] +
            [src.path for src in srcs if src.extension == "purs"],
    )

purescript_bundle = rule(
    implementation = _purescript_bundle,
    attrs = {
        "library": attr.label(
            allow_single_file = True,
        ),
        "purs": attr.label(
            allow_single_file = True,
            executable = True,
            cfg = "host",
            default = "@purs",
        ),
    },
    outputs = {
        "js": "%{name}.js",
    },
)

purescript_library = rule(
    implementation = _purescript_library,
    attrs = {
        "srcs": attr.label_list(
            allow_files = True,
        ),
        "purs": attr.label(
            allow_single_file = True,
            executable = True,
            cfg = "host",
            default = "@purs",
        ),
    },
    outputs = {
        "tar": "%{name}.tar",
    },
)
